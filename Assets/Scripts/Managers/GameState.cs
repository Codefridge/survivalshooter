﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Static central game state
/// </summary>
public static class GameState {
    private static int Enemies = 0;

    #region Enemies
    public static void AddEnemy(int amount = 1)
    {
        Enemies += amount;
    }

    public static void RemoveEnemy(int amount = 1)
    {
        Enemies -= amount;
    }

    public static int NumberOfEnemies()
    {
        return Enemies;
    }
    #endregion
}
