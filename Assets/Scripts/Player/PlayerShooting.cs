﻿using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    public int damagePerShot = 20;
    public float mgTimeBetweenBullets = 0.15f;
    [Range(0f, 0.5f)]
    public float mgSpread = 0.2f;
    public float railgunTimeBetweenShots = 0.5f;
    public float range = 100f;
    public GameObject grenadeObject;


    float timer;
    float throwableTimer;
    Ray shootRay = new Ray();
    RaycastHit shootHit;
    int shootableMask;
    ParticleSystem gunParticles;
    LineRenderer gunLine;
    AudioSource gunAudio;
    Light gunLight;
    float effectsDisplayTime = 0.2f;

    int weaponSelected = 1;

    #region Getters and Setters
    public int GetWeaponSelected()
    {
        return weaponSelected;
    }
    #endregion

    void Awake ()
    {
        shootableMask = LayerMask.GetMask ("Shootable");
        gunParticles = GetComponent<ParticleSystem> ();
        gunLine = GetComponent <LineRenderer> ();
        gunAudio = GetComponent<AudioSource> ();
        gunLight = GetComponent<Light> ();
    }


    void Update ()
    {
        // Weapon selection
        if (Input.GetKey("1")) {
            weaponSelected = 1;
        } else if (Input.GetKey("2")) {
            weaponSelected = 2;
        } else if (Input.GetKey("3")) {
            weaponSelected = 3;
        }

        // Grenades
        if (Input.GetButton("Throwable"))
        {
            SpawnAndThrowGrenade();
        }

        if (weaponSelected == 1) {
            // Machine gun
            timer += Time.deltaTime;
            if(Input.GetButton ("Fire1") && timer >= mgTimeBetweenBullets && Time.timeScale != 0)
            {
                ShootMachineGun ();
            }

            if(timer >= mgTimeBetweenBullets * effectsDisplayTime)
            {
                DisableEffects ();
            }
        } else if (weaponSelected == 2) {
            // Railgun
            timer += Time.deltaTime;
            if(Input.GetButton ("Fire1") && timer >= railgunTimeBetweenShots && Time.timeScale != 0)
            {
                ShootRailgun ();
            }

            if(timer >= railgunTimeBetweenShots * effectsDisplayTime)
            {
                DisableEffects ();
            }
        }
    }

    private void SpawnAndThrowGrenade()
    {
        var grenade = Instantiate<GameObject>(grenadeObject);
        grenade.transform.position = transform.position;
        grenade.GetComponent<Rigidbody>().AddForce(transform.forward, ForceMode.Acceleration);
        grenade.SendMessage("StartGrenade", gameObject);
    }

    public void DisableEffects ()
    {
        gunLine.enabled = false;
        gunLight.enabled = false;
    }

    private void ShootMachineGun ()
    {
        timer = 0f;

        gunAudio.Play ();

        gunLight.enabled = true;

        gunParticles.Stop ();
        gunParticles.Play ();

        gunLine.enabled = true;
        gunLine.SetPosition (0, transform.position);

        //Randomness
        float randSpread = Random.Range((mgSpread / 2) * -1, mgSpread / 2);
        Vector3 direction = new Vector3(transform.forward.x + randSpread, transform.forward.y, transform.forward.z + randSpread);
        Debug.Log(direction + " : " + randSpread);

        shootRay.origin = transform.position;
        shootRay.direction = direction;
        
        if(Physics.Raycast (shootRay, out shootHit, range, shootableMask))
        {
            EnemyHealth enemyHealth = shootHit.collider.GetComponent <EnemyHealth> ();
            if(enemyHealth != null)
            {
                enemyHealth.EnemyTakeDamage (damagePerShot, shootHit.point);
            }
            gunLine.SetPosition (1, shootHit.point);
        }
        else
        {
            gunLine.SetPosition (1, shootRay.origin + shootRay.direction * range);
        }
    }

    private void ShootRailgun()
    {
        timer = 0f;

        gunAudio.Play ();

        gunLight.enabled = true;

        gunParticles.Stop ();
        gunParticles.Play ();

        gunLine.enabled = true;
        gunLine.SetPosition (0, transform.position);

        shootRay.origin = transform.position;
        shootRay.direction = transform.forward;

        if(Physics.Raycast (shootRay, out shootHit, range, shootableMask))
        {
            EnemyHealth enemyHealth = shootHit.collider.GetComponent <EnemyHealth> ();
            if(enemyHealth != null)
            {
                enemyHealth.EnemyTakeDamage (damagePerShot, shootHit.point);
            }
            gunLine.SetPosition (1, shootHit.point);
        }
        else
        {
            gunLine.SetPosition (1, shootRay.origin + shootRay.direction * range);
        }
    }

}
