﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponGrenade : MonoBehaviour {

    public int damage = 30;
    public float grenadeExplosionForce = 100f;
    public float damageRadius = 30f;
    public int startGrenadeAmount = 3;

    GameObject player;
    PlayerHealth playerHealth;
    bool exploding = false;
    float timeStarted = 0f;


    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerHealth = player.GetComponent<PlayerHealth>();
    }

    // Use this for initialization
    void Start () {
		
	}
    
    public void StartGrenade(GameObject spawnedBy)
    {
        if (timeStarted == 0)
            timeStarted = Time.fixedTime;
    }
	
	// Update is called once per frame
	void Update () {
        if (timeStarted + 1 <= Time.fixedTime)
        {
            exploding = true;
            GetComponent<Rigidbody>().AddExplosionForce(grenadeExplosionForce, transform.position, damageRadius);
            // TODO: I need to keep track of this force and disable it
        }

        if (!exploding)
        return;

		Collider[] hitColliders = Physics.OverlapSphere(gameObject.transform.position, damageRadius);
        int i = 0;
        while (i < hitColliders.Length)
        {
            hitColliders[i].SendMessage("EnemyTakeDamage", damage, SendMessageOptions.DontRequireReceiver);
            i++;
        }
	}

    void OnDrawGizmosSelected()
    {
        // Display the explosion radius when selected
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, damageRadius);
    }

}
